set unstable

export POETRY_VIRTUALENVS_IN_PROJECT := "false"

[private]
default:
    just --list

[private]
pre-commit:
    -pre-commit install  --hook-type pre-commit --hook-type pre-push

[group('virtualenv')]
poetry-install: pre-commit
    poetry install

mypy: poetry-install
    poetry run mypy --strict --install-types --non-interactive .

[script('bash')]
test *options: poetry-install mypy
    set -euxo pipefail
    pytest_exe=$(poetry env info --path)/bin/pytest
    poetry run coverage run --rcfile={{ justfile_dir() }}/pyproject.toml --branch ${pytest_exe} {{ options }}

t *options: (test "--exitfirst --failed-first " + options)

# Display coverage from a test run
[script('bash')]
cover: test
    set -euxo pipefail
    poetry run coverage html --rcfile={{ justfile_dir() }}/pyproject.toml --show-contexts
    open htmlcov/index.html

clean:
    poetry env info --path | xargs --no-run-if-empty rm -rf
    git clean -dx --interactive

alias runme := run

run: test
    poetry run coverage run bridge/main.py
    just --no-deps cover
