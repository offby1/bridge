from __future__ import annotations

import itertools
from typing import TYPE_CHECKING

import pytest

from .auction import Auction
from .card import Card, Rank, Suit
from .contract import Bid, Pass
from .main import sample_deal, sample_play
from .seat import Seat
from .xscript import CBS, HandTranscript

if TYPE_CHECKING:
    from .table import Table


@pytest.fixture
def set_up_auction() -> tuple[Table, Auction, CBS]:
    table, cards_by_seat = sample_deal(shuffle_deck=False)
    auction = Auction(table=table, dealer=Seat.NORTH)
    player = itertools.cycle(table.players_by_seat.values())
    auction.append_located_call(player=next(player), call=Bid(level=1, denomination=Suit.CLUBS))
    auction.append_located_call(player=next(player), call=Pass)
    auction.append_located_call(player=next(player), call=Pass)
    auction.append_located_call(player=next(player), call=Pass)

    return table, auction, cards_by_seat


def test_serialization(set_up_auction: tuple[Table, Auction, CBS]) -> None:
    table, auction, cards_by_seat = set_up_auction

    xscript = sample_play(table, auction, dealt_cards_by_seat=cards_by_seat)

    s = xscript.serializable()
    roundtripped = HandTranscript.from_python(s)

    assert roundtripped.serializable() == s


def test_current_holding(set_up_auction: tuple[Table, Auction, CBS]) -> None:
    table, auction, cards_by_seat = set_up_auction

    xscript = HandTranscript(
        table, auction, ns_vuln=False, ew_vuln=False, dealt_cards_by_seat=cards_by_seat
    )

    seat_what_gonna_play = xscript.seat_cycle[0]
    chosen_play = xscript.slightly_less_dumb_play()
    xscript.add_card(chosen_play.card)

    ch = xscript.current_holdings[seat_what_gonna_play]
    assert ch is not None
    assert chosen_play.card not in ch


def test_whats_new(set_up_auction: tuple[Table, Auction, CBS]) -> None:
    table, auction, cards_by_seat = set_up_auction

    server_xscript = HandTranscript(
        table, auction, ns_vuln=False, ew_vuln=False, dealt_cards_by_seat=cards_by_seat
    )

    whatsnew = server_xscript.whats_new(num_calls=0, num_plays=0)

    assert whatsnew == {
        "calls": [
            [0, {"call": "1♣", "player": {"name": "Dave", "seat": "N"}}],
            [1, {"call": "Pass", "player": {"name": "Carrie", "seat": "E"}}],
            [2, {"call": "Pass", "player": {"name": "Marlene", "seat": "S"}}],
            [3, {"call": "Pass", "player": {"name": "Ken", "seat": "W"}}],
        ],
        "plays": [],
    }

    whatsnew = server_xscript.whats_new(num_calls=len(whatsnew["calls"]), num_plays=0)

    assert whatsnew == {
        "calls": [],
        "plays": [],
    }

    p = server_xscript.slightly_less_dumb_play()
    server_xscript.add_card(p.card)
    p = server_xscript.slightly_less_dumb_play()
    server_xscript.add_card(p.card)
    p = server_xscript.slightly_less_dumb_play()
    server_xscript.add_card(p.card)

    whatsnew = server_xscript.whats_new(num_calls=4, num_plays=2)

    assert whatsnew is not None

    assert whatsnew["calls"] == []
    assert whatsnew["plays"] == [
        [2, {"seat": "W", "card": "♠2", "wins_the_trick": False}],
    ]

    client_xscript = HandTranscript(
        table,
        Auction(table=table, dealer=Seat.NORTH),
        ns_vuln=False,
        ew_vuln=False,
        dealt_cards_by_seat=cards_by_seat,
    )
    assert len(client_xscript.auction.player_calls) != len(server_xscript.auction.player_calls)
    assert client_xscript.num_plays != server_xscript.num_plays

    whatsnew = server_xscript.whats_new(num_calls=0, num_plays=0)
    assert whatsnew is not None
    client_xscript.absorb_updates(whats_new=whatsnew)

    assert len(client_xscript.auction.player_calls) == len(server_xscript.auction.player_calls)
    assert client_xscript.num_plays == server_xscript.num_plays


def test_holdings(set_up_auction: tuple[Table, Auction, CBS]) -> None:
    table, auction, cards_by_seat = set_up_auction
    server_xscript = HandTranscript(
        table,
        auction,
        ns_vuln=False,
        ew_vuln=False,
        dealt_cards_by_seat=cards_by_seat,
    )

    censored_xscript = server_xscript.as_viewed_by(table.players_by_seat[Seat("N")])
    # declarer is Seat(1), holdings[0]
    holdings = list(censored_xscript.current_holdings.values())

    assert holdings
    assert holdings[0] is not None
    assert len(holdings[0]) == 13
    assert holdings[1] is holdings[2] is holdings[3] is None

    two_of_diamonds = Card(rank=Rank(2), suit=Suit.DIAMONDS)
    server_xscript.add_card(two_of_diamonds)

    censored_xscript = server_xscript.as_viewed_by(table.players_by_seat[Seat("E")])
    holdings = list(censored_xscript.current_holdings.values())

    assert holdings[1] is not None
    assert len(holdings[1]) == 12
    assert two_of_diamonds not in holdings[1]
