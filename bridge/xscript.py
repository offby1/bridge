from __future__ import annotations

import collections
import dataclasses
import logging
from collections.abc import MutableMapping
from typing import TYPE_CHECKING, Any, Callable, Literal, cast

import colorama
import more_itertools

from .auction import Auction
from .card import Card, Suit
from .contract import Bid, Call, Contract
from .seat import Seat
from .table import Player, Table

if TYPE_CHECKING:
    from collections.abc import Iterator

logger = logging.getLogger(__name__)


class AuctionError(Exception):
    pass


class PlayError(Exception):
    pass


@dataclasses.dataclass(kw_only=True)
class Play:
    seat: Seat
    card: Card
    wins_the_trick: bool = False

    @classmethod
    def from_python(cls, data: dict[str, Any]) -> Play:
        return cls(
            seat=Seat.from_python(data["seat"]),
            card=Card.from_python(data["card"]),  # type: ignore
            wins_the_trick=data["wins_the_trick"],
        )

    def serializable(self) -> Any:
        return {
            "seat": self.seat.serializable(),
            "card": self.card.serializable(),
            "wins_the_trick": self.wins_the_trick,
        }

    def __repr__(self) -> str:
        return f"{self.seat.name}({self.seat.value}) ➙ {self.card!s}"

    def colorized(self) -> str:
        annotation = colorama.Fore.GREEN if self.wins_the_trick else colorama.Fore.RESET

        return annotation + repr(self) + colorama.Fore.RESET


@dataclasses.dataclass
class Trick:
    trump_suit: Suit | None
    plays: list[Play] = dataclasses.field(default_factory=list)

    @classmethod
    def from_python(cls, data: dict[str, Any]) -> Trick:
        trump_suit = data["trump_suit"]
        if trump_suit is not None:
            trump_suit = Suit.from_python(trump_suit)
        return cls(trump_suit=trump_suit, plays=[Play.from_python(p) for p in data["plays"]])

    def serializable(self) -> dict[str, Any]:
        ts = self.trump_suit
        trump_suit = ts.serializable() if ts is not None else None

        return {"plays": [p.serializable() for p in self.plays], "trump_suit": trump_suit}

    def __len__(self) -> int:
        return len(self.plays)

    def __getitem__(self, index: int) -> Play:
        return self.plays[index]

    def __iter__(self) -> Iterator[Play]:
        return iter(self.plays)

    def __repr__(self) -> str:
        if not self.plays:
            return f"<class {self.__class__.__name__}: empty>"
        return "<Trick " + ", ".join(map(str, self)) + ">"

    def append_play(self, player: Player, card: Card) -> None:
        if self.is_complete():
            msg = "Cannot add a play because the hand is complete"
            raise PlayError(msg)
        self.plays.append(Play(seat=player.seat, card=card))
        if self.is_complete():
            for p in self.plays:
                p.wins_the_trick = p.seat == self.winner(self.trump_suit).seat

    def is_complete(self) -> bool:
        return len(self.plays) == 4

    def winner(self, trump_suit: Suit | None) -> Play:
        assert self.is_complete()

        led_suit: Suit = self.plays[0].card.suit

        for suit in (trump_suit, led_suit):
            relevant_plays = []

            for play in self.plays:
                if play.card.suit is suit:
                    relevant_plays.append(play)

            if relevant_plays:
                return max(relevant_plays, key=lambda p: p.card.rank)

        msg = "Oops."
        raise Exception(msg)


# Conceptually this should be just a Mapping.  But clients need to update their transcript with the dummy's cards,
# *after* they've created it.
CBS = MutableMapping[Seat, list[Card] | None]


class HandTranscript:
    def __init__(
        self,
        table: Table,
        auction: Auction,
        ns_vuln: bool,
        ew_vuln: bool,
        dealt_cards_by_seat: CBS | None = None,
    ) -> None:
        self.table = table
        self.seat_cycle = list(
            self.table.players_by_seat,
        )  # not a real cycle, since you cannot examine those without changing them
        self.auction = auction
        self.tricks: list[Trick] = []
        if dealt_cards_by_seat is None:
            dealt_cards_by_seat = {}
        self.dealt_cards_by_seat = dealt_cards_by_seat

        self.ns_vuln = ns_vuln
        self.ew_vuln = ew_vuln

        assert len(self.table.players_by_seat) == 4

        self.rotate_seats_for_auction()

    def as_viewed_by(self, player: Player) -> HandTranscript:
        visible_seats = {player.seat}
        if self.num_plays > 0:
            visible_seats.add(self.auction.dummy.seat)
        dcbs = (
            {
                seat: cards if seat in visible_seats else None
                for seat, cards in self.dealt_cards_by_seat.items()
            }
            if self.dealt_cards_by_seat is not None
            else None
        )
        rv = HandTranscript(
            table=self.table,
            auction=self.auction,
            ns_vuln=self.ns_vuln,
            ew_vuln=self.ew_vuln,
            dealt_cards_by_seat=dcbs,
        )
        rv.tricks = self.tricks
        return rv

    @property
    def current_holdings(self) -> CBS:
        assert self.dealt_cards_by_seat is not None
        rv = {
            seat: None if cards is None else cards.copy()
            for seat, cards in self.dealt_cards_by_seat.items()
        }  # deep copy

        for p in self.plays():
            holding = rv[p.seat]

            if holding is not None:
                try:
                    holding.remove(p.card)
                except ValueError as e:
                    msg = "Can't play %s because %s"
                    raise PlayError(msg, p.card, e)

        return rv

    @classmethod
    def from_python(cls, data: dict[str, Any]) -> HandTranscript:
        table = Table.from_python(data["table"])

        # kludge alert: data["auction"] doesn't include the table, but we need the table to rehydrate the auction.
        auction = Auction.from_python({"auction": data["auction"], "table": data["table"]})

        tricks: list[Trick] = []
        # import pprint
        # pprint.pprint(data["tricks"])

        for t_python in data["tricks"]:
            t = Trick.from_python(t_python)
            assert t is not None
            tricks.append(t)
        ns_vuln = data["ns_vuln"]
        ew_vuln = data["ew_vuln"]
        rv = cls(
            table=table,
            auction=auction,
            ns_vuln=ns_vuln,
            ew_vuln=ew_vuln,
            dealt_cards_by_seat={
                Seat(direction): [Card.deserialize(s) for s in cardlist]
                if cardlist is not None
                else None
                for direction, cardlist in data["dealt_cards_by_seat"].items()
            },
        )
        rv.replay_tricks(tricks)
        return rv

    def serializable(self) -> Any:
        return {
            "auction": self.auction.serializable(),
            "dealt_cards_by_seat": {
                seat.value: [c.serializable() for c in cards] if cards is not None else None
                for seat, cards in self.dealt_cards_by_seat.items()
            },
            "ew_vuln": self.ew_vuln,
            "ns_vuln": self.ns_vuln,
            "table": self.table.serializable(),
            "tricks": [t.serializable() for t in self.tricks],
        }

    def plays(self) -> Iterator[Play]:
        for t in self.tricks:
            yield from t.plays

    @property
    def num_events(self) -> int:
        return self.num_plays + len(self.auction.player_calls)

    @property
    def num_plays(self) -> int:
        return int(more_itertools.ilen(self.plays()))

    def rotate_seats_for_auction(self) -> None:
        # TODO -- each of these "while" loops could be done with a single computation, followed by at most three
        # rotations.

        # rotate the seats so that the allowed caller (i.e., dealer) is in front
        while True:
            allowed_caller = self.auction.allowed_caller()
            if allowed_caller is None or self.seat_cycle[0] == allowed_caller.seat:
                break
            self.rotate_seats()

        if self.auction.found_contract:
            assert self.auction.declarer is not None
            while self.seat_cycle[0] != self.auction.declarer.seat.lho():
                self.rotate_seats()

    def rotate_seats(self) -> None:
        self.seat_cycle.append(self.seat_cycle.pop(0))

    def add_call(self, call: Call) -> None:
        if self.auction.status is not Auction.Incomplete:
            msg = f"{self.auction.status=} is not Auction.Incomplete"
            raise AuctionError(msg)

        self.auction.append_located_call(
            player=self.table.players_by_seat[self.seat_cycle[0]],
            call=call,
        )

        self.rotate_seats_for_auction()

    def add_card(self, card: Card) -> None:
        """Adds a card to the transcript."""
        if not isinstance(self.auction.status, Contract):
            msg = "Cannot play a card if there is no contract"
            raise PlayError(msg)
        if not self.tricks or self.tricks[-1].is_complete():
            self.tricks.append(Trick(self.auction.status.bid.denomination))

        self.tricks[-1].append_play(self.table.players_by_seat[self.seat_cycle[0]], card)

        self.rotate_seats()

        if self.tricks[-1].is_complete():
            while True:
                n = self.next_seat_to_play()
                if n is None:
                    assert len(self.tricks) == 13
                    break
                if self.seat_cycle[0] == n:
                    break

                self.rotate_seats()

    def replay_tricks(self, tricks: list[Trick]) -> None:
        for t in tricks:
            for p in t:
                self.add_card(p.card)

    def whats_new(self, *, num_calls: int, num_plays: int) -> dict[str, Any] | None:
        new_calls = []
        if num_calls < len(self.auction.player_calls):
            new_calls = [
                [index, c.serializable()]
                for index, c in list(enumerate(self.auction.player_calls))[num_calls:]
            ]
        new_plays = []
        if num_plays < self.num_plays:
            new_plays = [
                [index, p.serializable()] for index, p in list(enumerate(self.plays()))[num_plays:]
            ]
        return {"calls": new_calls, "plays": new_plays}

    def absorb_updates(self, whats_new: dict[str, Any]) -> None:
        calls_added = cards_added = 0
        for index, c in whats_new["calls"]:
            if index == len(self.auction.player_calls):
                self.add_call(Bid.deserialize(c["call"]))
                calls_added += 1
                logger.debug(f"Just absorbed {c} with {index=}")
            else:
                logger.warning(
                    f"Cannot absorb call with {index=} because it is not exactly equal to {len(self.auction.player_calls)=}",
                )
        for index, p in whats_new["plays"]:
            logger.debug(f"Maybe absorbing {Play.from_python(p)} with {index=}; {self.num_plays=}")
            if index == self.num_plays:
                self.add_card(Card.deserialize(p["card"]))
                cards_added += 1
            else:
                logger.warning(
                    f"Cannot absorb play with {index=} because it is not exactly equal to {self.num_plays=}",
                )
        logger.debug("Just absorbed %s calls and %s cards", calls_added, cards_added)

    def is_complete(self) -> bool:
        if self.auction.status == self.auction.PassedOut:
            return True
        if len(self.tricks) < 13:
            return False
        return self.tricks[12].is_complete()

    def final_score(self) -> BrokenDownScore | None | Literal[0]:
        if not self.is_complete():
            return None
        if self.auction.status == self.auction.PassedOut:
            return 0
        tricks_by_seat: dict[Seat, list[Trick]] = collections.defaultdict(list)
        for t in self.tricks:
            contract = cast(Contract, self.auction.status)
            winner = t.winner(
                contract.bid.denomination,
            )  # sucks that we have to recompute this; the xscript should cache this

            tricks_by_seat[winner.seat].append(t)

        east_west_tricks = 0
        north_south_tricks = 0
        for seat, tricks in tricks_by_seat.items():
            if seat in (Seat.EAST, Seat.WEST):
                east_west_tricks += len(tricks)
            else:
                north_south_tricks += len(tricks)

        a = self.auction
        c = a.status
        assert isinstance(c, Contract)
        assert c.declarer is not None
        declarer_tricks = (
            north_south_tricks if c.declarer.seat in (Seat.NORTH, Seat.SOUTH) else east_west_tricks
        )

        assert self.auction.declarer is not None
        declarer_vulnerable = (
            self.auction.declarer.seat in (Seat.NORTH, Seat.SOUTH) and self.ns_vuln
        ) or (self.auction.declarer.seat in (Seat.EAST, Seat.WEST) and self.ew_vuln)

        broken_down_score = BrokenDownScore(
            declarer_vuln=declarer_vulnerable,
            declarer_tricks=declarer_tricks,
            contract=c,
        )

        broken_down_score.east_west_tricks = east_west_tricks
        broken_down_score.north_south_tricks = north_south_tricks

        if c.declarer.seat in (Seat.NORTH, Seat.SOUTH):
            if broken_down_score.total < 0:
                broken_down_score.east_west_points = -broken_down_score.total
                broken_down_score.north_south_points = 0
            else:
                broken_down_score.east_west_points = 0
                broken_down_score.north_south_points = broken_down_score.total
        elif broken_down_score.total < 0:
            broken_down_score.east_west_points = 0
            broken_down_score.north_south_points = -broken_down_score.total
        else:
            broken_down_score.east_west_points = broken_down_score.total
            broken_down_score.north_south_points = 0
        return broken_down_score

    def next_seat_to_play(self) -> Seat | None:
        if not isinstance(self.auction.status, Contract):
            return None
        if self.is_complete():
            return None
        contract = self.auction.status
        assert contract.declarer is not None
        if not self.tricks or not self.tricks[0].plays:
            lho = self.table.get_lho(contract.declarer)
            return lho.seat
        most_recent_trick = self.tricks[-1]
        if most_recent_trick.is_complete():
            trump_suit = contract.bid.denomination
            return most_recent_trick.winner(trump_suit).seat
        last_card_played = most_recent_trick.plays[-1]
        return self.table.get_lho_seat(last_card_played.seat)

    def legal_cards(self, *, some_cards: list[Card]) -> list[Card]:
        if not self.tricks or self.tricks[-1].is_complete():
            return some_cards

        if not self.tricks[-1].plays:
            return some_cards

        led_suit = self.tricks[-1].plays[0].card.suit
        cards_of_that_suit = [c for c in some_cards if c.suit == led_suit]
        if cards_of_that_suit:
            return cards_of_that_suit

        return some_cards

    # TODO -- this is probably redundant with something else in this library; see if they can be combined
    def would_beat(self, *, candidate: Card, subject: Card) -> bool:
        assert isinstance(self.auction.status, Contract)
        trump_suit = self.auction.status.bid.denomination

        if candidate.suit == subject.suit:
            return candidate.rank > subject.rank

        if candidate.suit == trump_suit:
            return True

        if subject.suit == trump_suit:
            return False

        return False

    # Note that this does *not* call `add_card`; callers need to do that themselves.
    def slightly_less_dumb_play(self, *, order_func: Callable[[Card], int] | None = None) -> Play:
        if order_func is None:

            def order_func(_: Card) -> int:
                return 0

        logger.debug("Current player is %s", self.seat_cycle[0])
        some_cards = self.current_holdings[self.seat_cycle[0]]
        assert some_cards is not None
        options = self.legal_cards(some_cards=some_cards)

        # Sensible default, I guess
        chosen_card = min(options, key=order_func)

        if not self.tricks:
            # opening lead
            # Play a known winner if we've got one.
            my_victors = [
                candidate
                for candidate in sorted(options, key=order_func)
                if order_func(candidate) == 0
            ]
            if my_victors:
                chosen_card = my_victors[0]

        else:
            last_trick = self.tricks[-1]
            if len(last_trick) == 1:
                # If we have a card that will beat RHO's card, then play it.
                # TODO -- if we're declarer, take dummy into account.
                opps = last_trick[-1].card

                my_victors = [
                    candidate
                    for candidate in sorted(options, key=order_func)
                    if self.would_beat(candidate=candidate, subject=opps)
                ]
                if my_victors:
                    chosen_card = my_victors[0]

                if chosen_card <= opps:
                    chosen_card = min(options, key=order_func)

            elif len(last_trick) == 2:
                # third hand high ... unless it's lower than the opponent's card
                opps = last_trick[1].card

                chosen_card = max(options, key=order_func)

                # TODO -- don't overtake partner for no reason
                # i.e., if their card is clearly going to win, just revert to the minimum
                if not self.would_beat(candidate=chosen_card, subject=opps):
                    chosen_card = min(options, key=order_func)

            elif len(last_trick) == 3:
                # Attempt to win the trick if either opponent's card is more powerful than partner's, and if we have a
                # card that is more powerful than all of them
                opps_cards: tuple[Card, Card] = (last_trick[0].card, last_trick[2].card)
                partners_card: Card = last_trick[1].card
                partner_gonna_win = all(
                    self.would_beat(candidate=partners_card, subject=opp) for opp in opps_cards
                )

                if not partner_gonna_win:
                    # See if we have a card that could win; if so, play the weakest.
                    my_victors = [
                        candidate
                        for candidate in sorted(options, key=order_func)
                        if all(
                            self.would_beat(candidate=candidate, subject=oppo)
                            for oppo in opps_cards
                        )
                    ]
                    if my_victors:
                        chosen_card = my_victors[0]

        return Play(seat=self.seat_cycle[0], card=chosen_card)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}, {len(self.auction.player_calls)} calls, {self.num_plays} plays, {self.seat_cycle[0].name=}"

    def __str__(self) -> str:
        return repr(self)


@dataclasses.dataclass
class BrokenDownScore:
    """I exist partly for testability, and partly because it might be nice for a UI to explain a final score.

    See https://en.wikipedia.org/wiki/Bridge_scoring
    """

    contract_points: int
    overtrick_points: int
    slam_bonus: int
    doubled_bonus: int
    penalty_points: int
    game_bonus: int

    trick_summary: str

    # Redundant but handy.  Callers set these themselves.
    east_west_points: int = 0
    north_south_points: int = 0
    east_west_tricks: int = 0
    north_south_tricks: int = 0

    def __init__(self, *, declarer_vuln: bool, declarer_tricks: int, contract: Contract) -> None:
        odd_tricks = 0 if declarer_tricks < contract.bid.level + 6 else contract.bid.level

        mul = 1 if contract.multiplier is None else contract.multiplier

        if contract.bid.denomination is None:
            contract_points = (odd_tricks * 30) * mul
            if contract_points > 0:
                contract_points += 10 * mul
        elif contract.bid.denomination in (Suit.HEARTS, Suit.SPADES):
            contract_points = (odd_tricks * 30) * mul
        else:
            contract_points = (odd_tricks * 20) * mul

        if declarer_tricks < contract.bid.level + 6:
            overtricks = 0
        elif declarer_tricks == contract.bid.level + 6:
            overtricks = 0
            self.trick_summary = "Made contract exactly"
        else:
            overtricks = declarer_tricks - 6 - odd_tricks
            self.trick_summary = f"Made contract with {overtricks} overtricks"

        if mul == 1:
            if contract.bid.denomination in (Suit.CLUBS, Suit.DIAMONDS):
                overtrick_points = overtricks * 20
            else:
                overtrick_points = overtricks * 30

        else:
            overtrick_points = overtricks * (50 * (declarer_vuln + 1)) * mul

        slam_bonus = 0

        if odd_tricks == 6:
            slam_bonus = 750 if declarer_vuln else 500
        elif odd_tricks == 7:
            slam_bonus = 1500 if declarer_vuln else 1000

        doubled_bonus = 0
        if odd_tricks > 0:
            if mul == 2:
                doubled_bonus = 50
            elif mul == 4:
                doubled_bonus = 100

        undertricks = (
            0
            if declarer_tricks >= contract.bid.level + 6
            else contract.bid.level + 6 - declarer_tricks
        )

        if undertricks > 0:
            self.trick_summary = f"Down {undertricks}"

        points_per_undertrick_by_vuln = [
            {False: {1: 50, 2: 100, 4: 200}, True: {1: 100, 2: 200, 4: 400}},  # 1st undertrick
            {False: {1: 50, 2: 200, 4: 400}, True: {1: 100, 2: 300, 4: 600}},  # 2nd and 3rd, each
            {False: {1: 50, 2: 200, 4: 400}, True: {1: 100, 2: 300, 4: 600}},  # 2nd and 3rd, each
            {
                False: {1: 50, 2: 300, 4: 600},
                True: {1: 100, 2: 300, 4: 600},
            },  # 4th and each subsequent
        ]
        penalty_points = 0
        for which_undertrick_starting_with_1 in range(1, undertricks + 1):
            list_index = min(
                which_undertrick_starting_with_1 - 1,
                len(points_per_undertrick_by_vuln) - 1,
            )
            penalty_points += points_per_undertrick_by_vuln[list_index][declarer_vuln][mul]

        game_bonus = 0
        if odd_tricks > 0:
            game_bonus = 50 if contract_points < 100 else 500 if declarer_vuln else 300

        self.contract_points = contract_points
        self.overtrick_points = overtrick_points
        self.slam_bonus = slam_bonus
        self.doubled_bonus = doubled_bonus
        self.penalty_points = penalty_points
        self.game_bonus = game_bonus

    def __str__(self) -> str:
        return repr(self) + ": " + self.trick_summary

    @property
    def total(self) -> int:
        return sum(
            [
                self.contract_points,
                self.overtrick_points,
                self.slam_bonus,
                self.doubled_bonus,
                -self.penalty_points,
                self.game_bonus,
            ],
        )
