from __future__ import annotations

import dataclasses
import functools
import itertools
from typing import TYPE_CHECKING, Literal

from . import _assert_type, card

if TYPE_CHECKING:
    from .table import Player


class DeserializationException(Exception):
    pass


class Call:
    def __init__(self, name: str) -> None:
        self.name = name

    def __repr__(self) -> str:
        return self.name

    def str_for_bidding_box(self) -> str:
        return self.name

    def serialize(self, as_html: bool = False) -> str:
        return self.name

    serializable = serialize

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Call):
            return False
        return getattr(self, "name", None) == getattr(o, "name", None)

    @classmethod
    def deserialize(cls, s: str) -> Call | None:
        for candidate in Pass, Double, Redouble:
            if s == candidate.name:
                return candidate
        return None

    from_python = deserialize


Pass = Call("Pass")
Double = Call("Double")
Redouble = Call("Redouble")

Multiplier = Literal[None, 2, 4]


@functools.total_ordering
@dataclasses.dataclass(kw_only=True)
class Bid(Call):
    level: int
    denomination: card.Suit | None  # None means "notrump", of course

    num2words = {
        1: "one",
        2: "two",
        3: "three",
        4: "four",
        5: "five",
        6: "six",
        7: "seven",
    }

    def str_for_bidding_box(self) -> str:
        den = self.denomination if self.denomination is not None else "N"
        return f"{self.level}{den}"

    def __str__(self) -> str:
        den = (
            self.denomination.name(plural=self.level > 1)
            if self.denomination is not None
            else "notrump"
        )
        return f"{self.num2words[self.level]} {den}"

    def __lt__(self, other: Bid) -> bool:
        _assert_type(other, self.__class__)
        if self.level < other.level:
            return True
        if self.level > other.level:
            return False

        our_denomination = 5 if self.denomination is None else self.denomination
        their_denomination = 5 if other.denomination is None else other.denomination

        return our_denomination < their_denomination

    @classmethod
    def all_exceeding(cls, base: Bid | None = None) -> list[Bid]:
        pairs = itertools.product(
            range(1, 8),
            [*list(card.Suit.__members__.values()), None],
        )
        bids = [cls(level=lev, denomination=d) for lev, d in pairs]
        if base is None:
            return bids
        return [b for b in bids if b > base]

    @classmethod
    def max(cls) -> Bid:
        return cls(level=7, denomination=None)

    def serialize(self, as_html: bool = False) -> str:
        denomination_str = "N"  # notrump
        if self.denomination is not None:
            denomination_str = str(self.denomination)

        text = f"{self.level}{denomination_str}"

        if not as_html:
            return text

        color = "red" if self.denomination in {card.Suit.HEARTS, card.Suit.DIAMONDS} else "black"
        return f"""<span style="color: {color};">{text}</span>"""

    serializable = serialize

    @classmethod
    def deserialize(cls, s: str) -> Call:
        from_super = super().deserialize(s)
        if from_super is not None:
            return from_super

        try:
            level = int(s[0])

            if s[1] == "N":
                denomination = None  # notrump
            else:
                denomination = card.Suit(card.suit_number_from_glyph(s[1]))

            return cls(level=level, denomination=denomination)
        except Exception as e:
            msg = f"Dunno how to make a {cls=} from {s=}"
            raise DeserializationException(msg) from e

    from_python = deserialize


@dataclasses.dataclass(kw_only=True)
class Contract:
    bid: Bid
    multiplier: Multiplier
    declarer: Player

    def __str__(self) -> str:
        double_description = ""

        if self.multiplier == 2:
            double_description = ", doubled,"
        elif self.multiplier == 4:
            double_description = ", redoubled,"

        return f"{self.bid}{double_description} played by {self.declarer}"
