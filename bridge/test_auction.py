from __future__ import annotations

import itertools
import pickle
from typing import Sequence

import pytest

from .auction import Auction, IllegalCall, OutOfTurn, PlayerCall
from .card import Suit
from .contract import Bid, Call, Double, Pass, Redouble
from .main import sample_deal
from .seat import Seat
from .table import Hand, Player, Table


def auc(calls: Sequence[Call], dealer: Seat | None = None) -> Auction:
    if dealer is None:
        dealer = Seat.NORTH

    dave = Player(seat=Seat.NORTH, name="Dave")
    carrie = Player(seat=Seat.EAST, name="Carrie")
    marlene = Player(seat=Seat.SOUTH, name="Marlene")
    ken = Player(seat=Seat.WEST, name="Ken")

    players = [dave, carrie, marlene, ken]
    while players[0].seat != dealer:
        players.append(players.pop(0))
    rv = Auction(table=Table(players=players), dealer=dealer)
    for p, c in zip(itertools.cycle(rv.table.players_by_seat.values()), calls):
        rv.append_located_call(player=p, call=c)

    return rv


def test_passed_out() -> None:
    a = auc([Pass] * 4)
    status = a.status

    assert status is a.PassedOut


def test_unfinished() -> None:
    a = auc([Pass, Pass, Pass, Bid(level=3, denomination=Suit.CLUBS)])

    assert a.status is a.Incomplete


def test_one_club() -> None:
    a = auc([Bid(level=1, denomination=Suit.CLUBS), Pass, Pass, Pass])

    assert a.found_contract


def test_declarer_simplest_case() -> None:
    a = auc([Bid(level=1, denomination=Suit.CLUBS), Pass, Pass, Pass])

    assert a.declarer is not None
    assert a.declarer.seat.name == "North"


def test_declarer_rebid() -> None:
    a = auc(
        [
            Bid(level=1, denomination=Suit.CLUBS),  # N
            Pass,  # E
            Bid(level=2, denomination=Suit.CLUBS),  # S
            Pass,  # W
            Pass,  # N
            Pass,  # E
        ],
    )

    assert a.declarer is not None
    assert a.declarer.seat.name == "North"


def test_double() -> None:
    a = auc(
        [
            Pass,  # N
            Bid(level=1, denomination=None),  # E
            Double,  # S
            Pass,  # W
            Pass,  # N
            Pass,  # E
        ],
    )

    assert a.declarer is not None
    assert a.declarer.seat.name == "East"


def test_redoubled_immediately() -> None:
    a = auc(
        [
            Bid(level=1, denomination=None),  # N
            Double,  # E
            Redouble,  # S
            Pass,  # W
            Pass,  # N
            Pass,  # E
        ],
    )

    assert a.declarer is not None
    assert a.declarer.seat.name == "North"
    assert a.is_doubled == 4


def test_redoubled_after_two_passes() -> None:
    a = auc(
        [
            Bid(level=1, denomination=None),  # N
            Double,  # E
            Pass,  # S
            Pass,  # W
            Redouble,  # N
            Pass,  # E
            Pass,  # S
            Pass,  # W
        ],
    )

    assert a.declarer is not None
    assert a.declarer.seat.name == "North"
    assert a.is_doubled == 4


def test_exceptions_on_various_bogus_calls() -> None:
    a = auc([], dealer=Seat.NORTH)

    seat = a.dealer
    player = a.table.players_by_seat[seat]

    def advance() -> tuple[Seat, Player]:
        new_seat = seat.lho()
        player = a.table.players_by_seat[new_seat]
        return new_seat, player

    a.append_located_call(player=player, call=Pass)
    # Deliberately do *not* advance
    with pytest.raises(OutOfTurn):
        a.append_located_call(player=player, call=Pass)

    # OK, we've proven our point; advance now.
    seat, player = advance()

    a.append_located_call(player=player, call=Pass)
    seat, player = advance()

    with pytest.raises(IllegalCall):
        a.append_located_call(player=player, call=Double)

    a.append_located_call(player=player, call=Bid(level=1, denomination=Suit.CLUBS))
    seat, player = advance()


def test_random_legal_call() -> None:
    table, cards_by_seat = sample_deal(shuffle_deck=False)
    auction = Auction(table=table, dealer=Seat.NORTH)

    auction.append_located_call(
        player=table.players_by_seat[Seat.NORTH], call=Bid(denomination=None, level=7)
    )
    auction.append_located_call(player=table.players_by_seat[Seat.EAST], call=Pass)
    auction.random_legal_call()

    # No assertion; we're just checking that it hasn't raised an exception.


def test_fancy_two_dimensional_display() -> None:
    one_club = Bid(level=1, denomination=Suit.CLUBS)
    two_clubs = Bid(level=2, denomination=Suit.CLUBS)
    some_calls = [
        one_club,  # N
        Pass,  # E
        two_clubs,  # S
        Pass,  # W
        Pass,  # N
        Pass,  # E
    ]

    a = auc(calls=some_calls, dealer=Seat.NORTH)
    disp = a.fancy_two_dimensional_display()
    first_row = disp.pop(0)
    assert first_row == [None, one_club, Pass, two_clubs]

    second_row = disp.pop(0)
    assert not disp
    assert second_row == [Pass, Pass, Pass]

    a = auc(calls=some_calls, dealer=Seat.EAST)
    disp = a.fancy_two_dimensional_display()
    first_row = disp.pop(0)

    assert first_row == [None, None, one_club, Pass]

    second_row = disp.pop(0)
    assert not disp
    assert second_row == [two_clubs, Pass, Pass, Pass]


@pytest.mark.parametrize("include_hand", [False, True])
def test_hand_serialization(include_hand: bool) -> None:
    h = Hand(cards=[] if include_hand else None)
    s = h.serializable()
    roundtripped = Hand.from_python(s)

    assert roundtripped == h


def test_player_serialization() -> None:
    p = Player(seat=Seat.NORTH, name="Dave")
    s = p.serializable()
    roundtripped = Player.from_python(s)

    assert roundtripped == p


def test_serialization() -> None:
    pc = PlayerCall(
        call=Bid(level=1, denomination=Suit.CLUBS), player=Player(seat=Seat.NORTH, name="Dave")
    )
    s = pc.serializable()
    roundtripped = PlayerCall.from_python(s)

    assert roundtripped == pc


def test_equality() -> None:
    pass_ = Pass
    one_notrump = Bid(level=1, denomination=Suit.CLUBS)
    assert pass_ != one_notrump


def test_pickleization() -> None:
    pass_ = Pass
    s = pickle.dumps(pass_)
    roundtripped = pickle.loads(s)

    assert roundtripped == pass_

    a = auc([Pass] * 4)
    assert a.status is a.PassedOut
    s = pickle.dumps(a)
    roundtripped = pickle.loads(s)
    assert roundtripped.status is roundtripped.PassedOut


if __name__ == "__main__":
    pytest.main()
