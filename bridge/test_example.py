import json
import logging
import pathlib
from typing import Any

__here__ = pathlib.Path(__file__).parent.absolute()

from bridge.xscript import HandTranscript


def test_replaying_some_actual_data(caplog: Any) -> None:
    caplog.set_level(logging.DEBUG)

    with open(__here__ / "x.json") as inf:
        j = json.load(inf)

    x = HandTranscript.from_python(j)

    with open(__here__ / "updates-5-17.json") as inf:
        updates = json.load(inf)

    x.absorb_updates(updates)
    print(x)
