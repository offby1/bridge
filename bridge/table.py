from __future__ import annotations

import collections
import dataclasses
import logging
from typing import Any

import tabulate

from . import _assert_type
from .card import Card, Suit
from .seat import Seat

logger = logging.getLogger(__name__)


@dataclasses.dataclass(kw_only=True)
class Hand:
    cards: list[Card] | None = (
        None  # "None" doesn't mean "no cards"; rather, it means "we don't know because we're not allowed to see them".
    )

    @classmethod
    def from_python(cls, data: dict[str, list[Any] | None]) -> Hand:
        cards = (
            None
            if data["cards"] is None
            else [
                Card.from_python(s=c)  # type: ignore
                for c in data["cards"]
            ]
        )

        return cls(cards=cards)

    def serializable(self) -> dict[str, list[Any] | None]:
        if self.cards is None:
            return {"cards": None}
        return {"cards": [c.serializable() for c in self.cards]}

    # If our callers all used mypy, this wouldn't be necessary :-|
    def __post_init__(self) -> None:
        if self.cards is None:
            return
        for c in self.cards:
            _assert_type(c, Card)

    def append(self, card: Card) -> None:
        if self.cards is None:
            self.cards = []
        self.cards.append(card)

    def fancy_two_dimensional_display(self) -> list[str]:
        by_suit: dict[Suit, list[Card]] = collections.defaultdict(list)
        if self.cards is None:
            lines = [
                (Suit.SPADES.serializable(), "?"),
                (Suit.HEARTS.serializable(), "?"),
                (Suit.DIAMONDS.serializable(), "?"),
                (Suit.CLUBS.serializable(), "?"),
            ]
        else:
            for c in self.cards:
                by_suit[c.suit].append(c)

            # ensure each suit is represented, even if there's a void
            for s in list(Suit):
                if s not in by_suit:
                    by_suit[s] = []

            lines = []

            for suit, cards in sorted(by_suit.items(), reverse=True):
                assert isinstance(suit, Suit)
                cards = sorted(cards, reverse=True)
                card_str = "".join(str(c.rank) for c in cards) if cards else "-"
                lines.append((str(suit), card_str))

        return tabulate.tabulate(lines, tablefmt="orgtbl").split("\n")

    def __str__(self) -> str:
        if self.cards is None:
            return "?"
        return "".join(str(c) for c in self.cards)

    def __repr__(self) -> str:
        return repr(vars(self))


@dataclasses.dataclass(kw_only=True, frozen=True)
class Player:
    seat: Seat
    name: str

    @classmethod
    def from_python(cls, data: dict[str, Any]) -> Player:
        return cls(
            seat=Seat.from_python(data["seat"]),
            name=data["name"],
        )

    def serializable(self) -> dict[str, Any]:
        return {
            "name": self.name,
            "seat": self.seat.serializable(),
        }

    def __str__(self) -> str:
        return f"{self.name:>10}, sitting {self.seat:>5}"

    def __repr__(self) -> str:
        return f"libPlayer{vars(self)}"

    # If our callers all used mypy, this wouldn't be necessary :-|
    def __post_init__(self) -> None:
        _assert_type(self.seat, Seat)
        _assert_type(self.name, str)


class Table:
    def __init__(self, *, players: list[Player]) -> None:
        # I have the vague sense that I will need to find players by seat, and am sure that I'll iterate through the
        # players in order
        self.players_by_seat = collections.OrderedDict()
        for p in players:
            _assert_type(p, Player)
            _assert_type(p.seat, Seat)

            self.players_by_seat[p.seat] = p

    @classmethod
    def from_python(cls, data: list[Any]) -> Table:
        players = [Player.from_python(p) for p in data]
        return cls(players=players)

    def serializable(self) -> list[Any]:
        return [p.serializable() for p in self.players_by_seat.values()]

    def get_lho(self, player: Player) -> Player:
        assert isinstance(player, Player)
        lho_seat = player.seat.lho()
        return self.players_by_seat[lho_seat]

    def get_lho_seat(self, seat: Seat) -> Seat:
        assert isinstance(seat, Seat)
        lho_seat = seat.lho()
        return self.players_by_seat[lho_seat].seat

    def get_partner(self, player: Player) -> Player:
        return self.get_lho(self.get_lho(player))

    def get_partner_seat(self, seat: Seat) -> Seat:
        return self.get_lho_seat(self.get_lho_seat(seat))

    def get_rho(self, player: Player) -> Player:
        return self.get_partner(self.get_lho(player))

    def get_rho_seat(self, seat: Seat) -> Seat:
        return self.get_partner_seat(self.get_lho_seat(seat))

    def are_opponents(self, player_one: Player, player_two: Player) -> bool:
        return player_one.seat.is_opponent(player_two.seat)

    def __repr__(self) -> str:
        return repr(vars(self))

    def __str__(self) -> str:
        return "\n".join(str(p) for p in self.players_by_seat)
