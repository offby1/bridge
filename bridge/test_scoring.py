from __future__ import annotations

import itertools
from typing import Any, cast

from .auction import Auction
from .card import Suit
from .contract import Bid, Contract, Multiplier, Pass
from .main import sample_deal
from .seat import Seat
from .table import Player
from .xscript import BrokenDownScore, HandTranscript

# https://en.wikipedia.org/wiki/Bridge_scoring#Duplicate_bridge fwiw


def Kontract(*args: Any, **kwargs: Any) -> Contract:
    kwargs = kwargs | {"declarer": Player(seat=Seat.EAST, name="east")}
    return Contract(*args, **kwargs)


# https://en.wikipedia.org/wiki/Bridge_scoring#Contract_points
def test_contract_points() -> None:
    # Shorthand because vulnerability doesn't matter
    def ns(*, declarer_tricks: int, contract: Contract) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=False,
            declarer_tricks=declarer_tricks,
            contract=contract,
        )

    for multiplier in (1, 2, 4):
        bigM = cast(Multiplier, None if multiplier == 1 else multiplier)

        def c(level: int, denomination: Suit | None) -> Contract:
            return Kontract(
                bid=Bid(level=level, denomination=denomination),
                multiplier=bigM,
            )

        c1 = c(1, Suit.CLUBS)
        assert ns(declarer_tricks=7, contract=c1).contract_points == 20 * multiplier
        assert ns(declarer_tricks=8, contract=c1).contract_points == 20 * multiplier

        h1 = c(1, Suit.HEARTS)
        assert ns(declarer_tricks=7, contract=h1).contract_points == 30 * multiplier
        assert ns(declarer_tricks=8, contract=h1).contract_points == 30 * multiplier

        n1 = c(1, None)
        assert ns(declarer_tricks=7, contract=n1).contract_points == 40 * multiplier
        assert ns(declarer_tricks=8, contract=n1).contract_points == 40 * multiplier


# https://en.wikipedia.org/wiki/Bridge_scoring#Overtrick_points
def test_overtricks() -> None:
    def c(level: int, denomination: Suit | None, multiplier: int) -> Contract:
        return Kontract(
            bid=Bid(level=level, denomination=denomination),
            multiplier=cast(Multiplier, None if multiplier == 1 else multiplier),
        )

    # Undoubled
    c1 = c(1, Suit.CLUBS, multiplier=1)
    h1 = c(1, Suit.HEARTS, multiplier=1)
    n1 = c(1, None, multiplier=1)

    # Vulnerability doesn't matter for undoubled
    def ns(*, declarer_tricks: int, contract: Contract) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=False,
            declarer_tricks=declarer_tricks,
            contract=contract,
        )

    assert ns(declarer_tricks=7, contract=c1).overtrick_points == 0
    assert ns(declarer_tricks=8, contract=c1).overtrick_points == 20
    assert ns(declarer_tricks=9, contract=c1).overtrick_points == 40

    assert ns(declarer_tricks=7, contract=h1).overtrick_points == 0
    assert ns(declarer_tricks=8, contract=h1).overtrick_points == 30
    assert ns(declarer_tricks=9, contract=h1).overtrick_points == 60

    assert ns(declarer_tricks=7, contract=n1).overtrick_points == 0
    assert ns(declarer_tricks=8, contract=n1).overtrick_points == 30
    assert ns(declarer_tricks=9, contract=n1).overtrick_points == 60

    # Doubled & Redoubled
    for multiplier in (2, 4):
        c1 = c(1, Suit.CLUBS, multiplier=multiplier)
        h1 = c(1, Suit.HEARTS, multiplier=multiplier)
        n1 = c(1, None, multiplier=multiplier)

        for vuln in (False, True):

            def ns(*, declarer_tricks: int, contract: Contract) -> BrokenDownScore:
                return BrokenDownScore(
                    declarer_vuln=vuln,
                    declarer_tricks=declarer_tricks,
                    contract=contract,
                )

            for contract in c1, h1, n1:
                assert ns(declarer_tricks=7, contract=contract).overtrick_points == (
                    multiplier
                ) * 0 * (vuln + 1)
                assert ns(declarer_tricks=8, contract=contract).overtrick_points == (
                    multiplier
                ) * 50 * (vuln + 1)
                assert ns(declarer_tricks=9, contract=contract).overtrick_points == (
                    multiplier
                ) * 100 * (vuln + 1)


def test_slam_bonus() -> None:
    def c(level: int) -> Contract:
        return Kontract(
            bid=Bid(level=level, denomination=None),
            multiplier=None,
        )

    small = c(6)
    large = c(7)

    def ns(*, declarer_tricks: int, contract: Contract, declarer_vuln: bool) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=declarer_vuln,
            declarer_tricks=declarer_tricks,
            contract=contract,
        )

    assert ns(declarer_tricks=12, contract=small, declarer_vuln=False).slam_bonus == 500
    assert (
        ns(declarer_tricks=13, contract=small, declarer_vuln=False).slam_bonus == 500
    )  # overtricks are counted elsewhere
    assert ns(declarer_tricks=12, contract=small, declarer_vuln=True).slam_bonus == 750
    assert ns(declarer_tricks=13, contract=small, declarer_vuln=True).slam_bonus == 750
    assert ns(declarer_tricks=13, contract=large, declarer_vuln=False).slam_bonus == 1000
    assert ns(declarer_tricks=13, contract=large, declarer_vuln=True).slam_bonus == 1500


def test_doubling_bonus() -> None:
    def ns(*, declarer_tricks: int, contract: Contract) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=False,
            declarer_tricks=declarer_tricks,
            contract=contract,
        )

    doubled = Kontract(bid=Bid(level=1, denomination=None), multiplier=2)
    redoubled = Kontract(bid=Bid(level=1, denomination=None), multiplier=4)

    assert ns(declarer_tricks=7, contract=doubled).doubled_bonus == 50
    assert ns(declarer_tricks=8, contract=doubled).doubled_bonus == 50
    assert ns(declarer_tricks=7, contract=redoubled).doubled_bonus == 100
    assert ns(declarer_tricks=8, contract=redoubled).doubled_bonus == 100


# https://en.wikipedia.org/wiki/Bridge_scoring#Penalty_points
def test_penalty_points() -> None:
    def ns(*, declarer_tricks: int, declarer_vuln: bool, contract: Contract) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=declarer_vuln,
            declarer_tricks=declarer_tricks,
            contract=contract,
        )

    # Note that `expected_penalty` below is the total for the hand, not per-trick.

    # Not vulnerable, not doubled.
    one_notrump_not_doubled = Kontract(bid=Bid(level=1, denomination=None), multiplier=None)
    for declarer_tricks in (6, 5, 4):
        expected_penalty = (7 - declarer_tricks) * 50
        assert (
            ns(
                declarer_tricks=declarer_tricks,
                declarer_vuln=False,
                contract=one_notrump_not_doubled,
            ).penalty_points
            == expected_penalty
        )

    # Not vulnerable, doubled.
    one_notrump_doubled = Kontract(bid=Bid(level=1, denomination=None), multiplier=2)
    for declarer_tricks, expected_penalty in ((6, 100), (5, 300), (4, 500), (3, 800)):
        assert (
            ns(
                declarer_tricks=declarer_tricks,
                declarer_vuln=False,
                contract=one_notrump_doubled,
            ).penalty_points
            == expected_penalty
        )

    # Not vulnerable, redoubled.
    one_notrump_redoubled = Kontract(bid=Bid(level=1, denomination=None), multiplier=4)
    for declarer_tricks, expected_penalty in ((6, 200), (5, 600), (4, 1000), (3, 1600)):
        assert (
            ns(
                declarer_tricks=declarer_tricks,
                declarer_vuln=False,
                contract=one_notrump_redoubled,
            ).penalty_points
            == expected_penalty
        )

    # Vulnerable, not doubled.
    for declarer_tricks in (6, 5, 4, 3, 2, 1):
        expected_penalty = (7 - declarer_tricks) * 100
        assert (
            ns(
                declarer_tricks=declarer_tricks,
                declarer_vuln=True,
                contract=one_notrump_not_doubled,
            ).penalty_points
            == expected_penalty
        )

    # Vulnerable, doubled.
    for declarer_tricks, expected_penalty in ((6, 200), (5, 500), (4, 800)):
        assert (
            ns(
                declarer_tricks=declarer_tricks,
                declarer_vuln=True,
                contract=one_notrump_doubled,
            ).penalty_points
            == expected_penalty
        )

    # Vulnerable, redoubled.
    for declarer_tricks, expected_penalty in ((6, 400), (5, 1000), (4, 1600)):
        assert (
            ns(
                declarer_tricks=declarer_tricks,
                declarer_vuln=True,
                contract=one_notrump_redoubled,
            ).penalty_points
            == expected_penalty
        )


def test_game_bonus() -> None:
    def ns(*, vuln: bool, level: int) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=vuln,
            declarer_tricks=level + 6,
            contract=Kontract(
                bid=Bid(level=level, denomination=None),
                multiplier=None,
            ),
        )

    assert ns(vuln=False, level=2).game_bonus == 50
    assert ns(vuln=True, level=2).game_bonus == 50
    assert ns(vuln=False, level=3).game_bonus == 300
    assert ns(vuln=True, level=3).game_bonus == 500


def test_one_notrump() -> None:
    def ns(*, declarer_tricks: int, declarer_vuln: bool, contract: Contract) -> BrokenDownScore:
        return BrokenDownScore(
            declarer_vuln=declarer_vuln,
            declarer_tricks=declarer_tricks,
            contract=contract,
        )

    # Note that `expected_penalty` below is the total for the hand, not per-trick.

    # Not vulnerable, not doubled.
    one_nt_not_doubled = Kontract(bid=Bid(level=1, denomination=None), multiplier=None)
    bds = ns(declarer_tricks=6, declarer_vuln=False, contract=one_nt_not_doubled)

    assert bds.contract_points == 0
    assert bds.penalty_points == 50


def test_passed_out_hand_scores_as_a_buncha_zeroes() -> None:
    table, cards_by_seat = sample_deal(shuffle_deck=False)
    auction = Auction(table=table, dealer=Seat.NORTH)
    player = itertools.cycle(table.players_by_seat.values())
    auction.append_located_call(player=next(player), call=Pass)
    auction.append_located_call(player=next(player), call=Pass)
    auction.append_located_call(player=next(player), call=Pass)
    auction.append_located_call(player=next(player), call=Pass)

    x = HandTranscript(
        table=table,
        auction=auction,
        ns_vuln=False,
        ew_vuln=False,
        dealt_cards_by_seat=cards_by_seat,
    )

    assert x.final_score() == 0
