import enum
import itertools
from typing import Any, Iterator


class Seat(enum.Enum):
    NORTH = "N"
    EAST = "E"
    SOUTH = "S"
    WEST = "W"

    def serializable(self) -> str:
        return self.name[0]

    @classmethod
    def from_python(cls, data: Any) -> "Seat":
        if isinstance(data, int):
            return cls(data)
        if isinstance(data, str):
            return cls(data)
        msg = f"ok, wtf is {data=}"
        raise AssertionError(msg)

    @classmethod
    def cycle(cls) -> Iterator["Seat"]:
        return itertools.cycle(cls)

    def lho(self) -> "Seat":
        c = self.cycle()
        while next(c) != self:
            pass
        return next(c)

    def partner(self) -> "Seat":
        return self.lho().lho()

    def is_opponent(self, other: "Seat") -> bool:
        return other in (self.lho(), self.lho().partner())

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"libSeat {vars(self)}"

    @property
    def name(self) -> str:
        return super().name.capitalize()
