from bridge.card import Card, Rank, Suit


def test_str() -> None:
    s = Suit.CLUBS
    assert str(s) == "♣"
    assert f"{s}" == "♣"


def test_serialization() -> None:
    c1 = Card(suit=Suit.CLUBS, rank=Rank.TEN)
    c2 = Card.deserialize(c1.serialize())
    assert c1 == c2

    three_of_clubs = Card(suit=Suit.CLUBS, rank=Rank(3))
    for club in "♧♣cC":
        c = Card.deserialize(club + "3")
        assert c == three_of_clubs


def test_hashable() -> None:
    c1 = Card(suit=Suit.CLUBS, rank=Rank.TEN)
    c2 = Card.deserialize(c1.serialize())
    c3 = Card(suit=Suit.CLUBS, rank=Rank.TEN)
    c4 = Card(suit=Suit.DIAMONDS, rank=Rank.JACK)
    s = {c1, c2}
    assert c1 in s
    assert c2 in s
    assert c3 in s  # I guess?
    assert c4 not in s


def test_colors() -> None:
    c1 = Card(suit=Suit.CLUBS, rank=Rank.TWO)
    c2 = Card(suit=Suit.DIAMONDS, rank=Rank.TWO)
    c3 = Card(suit=Suit.HEARTS, rank=Rank.TWO)
    c4 = Card(suit=Suit.SPADES, rank=Rank.TWO)

    assert c1.color == c4.color == "black"
    assert c2.color == c3.color == "red"
