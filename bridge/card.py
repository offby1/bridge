import dataclasses
import enum
import functools
import itertools
from typing import List

_suit_numbers_by_suit_glyph_sets = {"♣♧cC": 0, "♦♢dD": 1, "♥♡hH": 2, "♠♤sS": 3}

SUIT_GLYPHS = "".join([s[0] for s in _suit_numbers_by_suit_glyph_sets])


def suit_number_from_glyph(glyph: str) -> int:
    suit = -1

    for glyphset, number in _suit_numbers_by_suit_glyph_sets.items():
        if glyph in glyphset:
            suit = number
            break

    if suit == -1:
        msg = f"Cannot deserialize {glyph=} as a suit; expected one of {SUIT_GLYPHS=}"
        raise Exception(
            msg,
        )
    return suit


class Suit(enum.IntEnum):
    CLUBS = 0
    DIAMONDS = 1
    HEARTS = 2
    SPADES = 3

    def __str__(self) -> str:
        return SUIT_GLYPHS[int(self.value)]

    def __format__(self, _: str) -> str:
        return str(self)

    def name(self, plural: bool = True) -> str:  # type: ignore[override]
        n: str = super().name
        if not plural:
            n = n[:-1]
        return str(n).capitalize()

    @classmethod
    def from_python(cls, data: str) -> "Suit":
        try:
            i = int(data)
            return cls(i)
        except ValueError:
            return cls(suit_number_from_glyph(data))

    def serializable(self) -> str:
        return SUIT_GLYPHS[self.value]

    @property
    def color(self) -> str:
        return "red" if self in {Suit.HEARTS, Suit.DIAMONDS} else "black"


class Rank(enum.IntEnum):
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13
    ACE = 14

    def __str__(self) -> str:
        if self.value > 9:
            return self.name[0]
        return str(self.value)

    def __format__(self, _: str) -> str:
        return str(self)

    @classmethod
    def from_character(cls, c: str) -> "Rank":
        if "2" <= c <= "9":
            return cls(int(c))

        return cls("TJQKA".index(c) + 10)

    @property
    def name(self) -> str:
        return str(super().name).capitalize()


# The total_ordering ignores the trump suit; it's intended only for sorting the cards in a hand, for display.
@functools.total_ordering
@dataclasses.dataclass(kw_only=True, frozen=True)
class Card:
    suit: Suit
    rank: Rank

    @property
    def color(self) -> str:
        return self.suit.color

    def __str__(self) -> str:
        return self.serialize()

    def serialize(self) -> str:
        return f"{self.suit}{self.rank}"

    serializable = serialize

    def __repr__(self) -> str:
        return str(self)

    @classmethod
    def deck(cls) -> List["Card"]:
        return [cls(suit=suit, rank=rank) for suit, rank in itertools.product(Suit, Rank)]

    @classmethod
    def deserialize(cls, s: str) -> "Card":
        return cls(suit=Suit(suit_number_from_glyph(s[0])), rank=Rank.from_character(s[1]))

    from_python = deserialize

    def __lt__(self, other: "Card") -> bool:
        return (self.suit, self.rank) < (other.suit, other.rank)
