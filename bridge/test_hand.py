from .card import Card, Rank, Suit
from .table import Hand


def test_typical_display() -> None:
    h = Hand()

    h.append(Card(suit=Suit.CLUBS, rank=Rank.TWO))
    h.append(Card(suit=Suit.DIAMONDS, rank=Rank.TWO))
    h.append(Card(suit=Suit.HEARTS, rank=Rank.TWO))
    h.append(Card(suit=Suit.SPADES, rank=Rank.TWO))

    actual = h.fancy_two_dimensional_display()
    assert len(actual) == 4


def test_display_with_a_void() -> None:
    h = Hand()

    h.append(Card(suit=Suit.CLUBS, rank=Rank.TWO))
    h.append(Card(suit=Suit.DIAMONDS, rank=Rank.TWO))
    h.append(Card(suit=Suit.SPADES, rank=Rank.TWO))

    actual = h.fancy_two_dimensional_display()

    assert actual == ["| ♠ | 2 |", "| ♥ | - |", "| ♦ | 2 |", "| ♣ | 2 |"]
