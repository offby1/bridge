from __future__ import annotations

import collections
import itertools
import random
import sys

import colorama
import tabulate

from bridge.auction import Auction
from bridge.card import Card
from bridge.seat import Seat
from bridge.table import Player, Table
from bridge.xscript import CBS, HandTranscript


def sample_deal(shuffle_deck: bool = True) -> tuple[Table, CBS]:
    dave = Player(seat=Seat.NORTH, name="Dave")
    carrie = Player(seat=Seat.EAST, name="Carrie")
    marlene = Player(seat=Seat.SOUTH, name="Marlene")
    ken = Player(seat=Seat.WEST, name="Ken")
    table = Table(players=[dave, carrie, marlene, ken])
    deck = Card.deck()
    if shuffle_deck:
        random.shuffle(deck)
    cards_by_seat = collections.defaultdict(list)

    cards_by_seat[Seat.NORTH].extend(deck[0:13])
    cards_by_seat[Seat.EAST].extend(deck[13:26])
    cards_by_seat[Seat.SOUTH].extend(deck[26:39])
    cards_by_seat[Seat.WEST].extend(deck[39:52])

    for seat in cards_by_seat:
        cards_by_seat[seat].sort()

    return table, dict(cards_by_seat)


def sample_auction(table: Table) -> Auction:
    auction = Auction(table=table, dealer=Seat.NORTH)

    # Am I sure the items are in the right order?
    for player in itertools.cycle(table.players_by_seat.values()):
        call = auction.random_legal_call()
        auction.append_located_call(player=player, call=call)

        status = auction.status

        if status != auction.Incomplete:
            break
    return auction


def sample_play(table: Table, auction: Auction, dealt_cards_by_seat: CBS) -> HandTranscript:
    xscript = HandTranscript(
        table, auction, ns_vuln=False, ew_vuln=False, dealt_cards_by_seat=dealt_cards_by_seat
    )

    if auction.found_contract:
        while True:
            play = xscript.slightly_less_dumb_play()
            xscript.add_card(play.card)

            if xscript.final_score():
                break

    else:
        pass

    return xscript


if __name__ == "__main__":
    colorama.init(autoreset=True)

    random.seed(0)
    table, cards_by_seat = sample_deal()

    a = sample_auction(table)
    for _pc in a.player_calls:
        sys.stdout.write(f"{_pc.player} sez {_pc.call}\n")

    xscript = sample_play(table, a, dealt_cards_by_seat=cards_by_seat)
    pt = tabulate.tabulate(
        [[p.colorized() for p in t.plays] for t in xscript.tricks],
        headers=("Leader", "LHO", "Partner", "RHO"),
        tablefmt="orgtbl",
    )
    sys.stdout.write(pt)
