from __future__ import annotations

import dataclasses
import logging
import random
from typing import Any, Callable, Iterable, Sequence, cast

from bridge import _assert_type
from bridge.contract import Bid, Call, Contract, Double, Multiplier, Pass, Redouble
from bridge.seat import Seat
from bridge.table import Player, Table

logger = logging.getLogger(__name__)


@dataclasses.dataclass(kw_only=True)
class PlayerCall:
    call: Call
    player: Player

    @classmethod
    def from_python(cls, data: dict[str, Any]) -> PlayerCall | None:
        call = Bid.from_python(s=data["call"])  # type: ignore
        if call is not None:
            return cls(player=Player.from_python(data["player"]), call=call)

        return None

    def serializable(self) -> dict[str, Any]:
        return {"call": self.call.serializable(), "player": self.player.serializable()}


def _tail_starting_at(
    seq: Any,
    sought_fn: Callable[[Any], bool],
) -> list[Any] | None:
    tail = []
    for elt in reversed(seq):
        tail.append(elt)

        if sought_fn(elt):
            return list(reversed(tail))

    return None


class AuctionException(Exception):
    pass


class IllegalCall(AuctionException):
    pass


class OutOfTurn(AuctionException):
    pass


def _is_bid(c: PlayerCall) -> bool:
    return isinstance(c.call, Bid)


@dataclasses.dataclass(kw_only=True)
class Auction:
    table: Table
    dealer: Seat
    player_calls: list[PlayerCall] = dataclasses.field(default_factory=list)
    is_doubled: Multiplier = None
    max_bid = None

    @classmethod
    def from_python(cls, data: dict[str, Any]) -> Auction:
        table = Table.from_python(data["table"])
        dealer = Seat.from_python(data["auction"]["dealer"])

        rv: Auction = cls(table=table, dealer=dealer)

        for pc_python in data["auction"]["player_calls"]:
            pc = PlayerCall.from_python(pc_python)
            if pc is not None:
                rv.append_located_call(player=pc.player, call=pc.call)

        return rv

    def serializable(self) -> Any:
        return {
            "dealer": self.dealer.serializable(),
            "player_calls": [pc.serializable() for pc in self.player_calls],
            "is_doubled": self.is_doubled,
        }

    # If our callers all used mypy, this wouldn't be necessary :-|
    def __post_init__(self) -> None:
        _assert_type(self.table, Table)
        _assert_type(self.dealer, Seat)

    class Incomplete:
        def __str__(self) -> str:
            return "Incomplete"

    class PassedOut:
        def __str__(self) -> str:
            return "Passed Out"

    def allowed_caller(self) -> Player | None:
        if self.status != self.Incomplete:
            return None

        # https://en.wikipedia.org/wiki/Contract_bridge#Auction
        # The dealer opens the auction and can make the first call, and the auction proceeds clockwise.

        cycle = Seat.cycle()

        # rotate a cycle until the dealer is first.
        while True:
            if self.dealer == next(cycle):
                allowed = self.dealer
                break
        # Now rotate once for each call that's been made so far.
        for _ in self.player_calls:
            allowed = next(cycle)

        return self.table.players_by_seat[allowed]

    def bidding_allowed(self) -> bool:
        if self.found_contract:
            return False
        return (self.max_bid is None) or (self.max_bid < Bid.max())

    def append_located_call(self, *, player: Player, call: Call) -> None:
        _assert_type(player, Player)
        _assert_type(call, Call)

        self.raise_if_illegal_call(player=player, call=call)

        if call is Double:
            self.is_doubled = 2
        elif call is Redouble:
            self.is_doubled = 4
        elif isinstance(call, Bid):
            self.max_bid = call
            self.is_doubled = None
        else:
            assert call is Pass

        self.player_calls.append(PlayerCall(call=call, player=player))

    @property
    def found_contract(self) -> bool:
        return self.status not in (self.PassedOut, self.Incomplete)

    @property
    def status(
        self,
    ) -> type[Auction.PassedOut | Auction.Incomplete] | Contract:
        lcs = self.player_calls
        # every player has had a chance to call
        if len(lcs) < len(Seat):
            return self.Incomplete

        # the last four calls were "pass"
        if all(lc.call == Pass for lc in lcs):
            return self.PassedOut

        # If there are any bids at all, and the last three calls were Pass, we're done.
        tail_calls = _tail_starting_at(lcs, _is_bid)
        if tail_calls and len(tail_calls) > 3:
            last_three = tail_calls[-3:]

            if all(c.call.name == "Pass" for c in last_three):

                def is_same_denomination_bid_by_winning_partnership(
                    player_call: PlayerCall,
                ) -> bool:
                    # Not a bid.
                    if not isinstance(player_call.call, Bid):
                        return False

                    # Not made by the winning partnership.
                    if player_call.player not in (
                        tail_calls[0].player,
                        self.table.get_partner(tail_calls[0].player),
                    ):
                        return False

                    # Not the same denomination.
                    if player_call.call.denomination != tail_calls[0].call.denomination:
                        return False

                    # Well, ok then!
                    return True

                def _first(
                    seq: Iterable[PlayerCall], sought_fn: Callable[[PlayerCall], bool]
                ) -> PlayerCall:
                    for elt in seq:
                        if sought_fn(elt):
                            return elt

                    msg = f"Could not find {sought_fn=} in {seq=}"
                    raise AssertionError(msg)

                declarer: Player = _first(
                    lcs, is_same_denomination_bid_by_winning_partnership
                ).player

                return Contract(
                    bid=tail_calls[0].call,
                    multiplier=self.is_doubled,
                    declarer=declarer,
                )

        return self.Incomplete

    @property
    def declarer(self) -> Player | None:
        # These first asserts are to get nice human-readable messages; the last is to mollify
        # mypy
        assert self.found_contract

        status = self.status

        assert isinstance(status, Contract), f"OK, things are really hairy here; wtf is {status=}?"

        return status.declarer

    @property
    def dummy(self) -> Player:
        assert self.declarer is not None
        return self.table.get_partner(self.declarer)

    def raise_if_illegal_call(self, *, player: Player, call: Call) -> None:
        if player != self.allowed_caller():
            msg = f"{player} isn't allowed to call now; only {self.allowed_caller()} is"
            raise OutOfTurn(msg)
        if call not in self.legal_calls():
            msg = f"{call=} ain't legal"
            logger.debug("%s", self.serializable())
            raise IllegalCall(msg)

    @property
    def last_located_bid(self) -> PlayerCall | None:
        tail_calls = _tail_starting_at(self.player_calls, _is_bid)
        if tail_calls is None:
            return None
        return cast(PlayerCall, tail_calls[0])

    # TODO -- turn this into a generator?  I can imagine callers being interested only in the first few items, and it
    # seems wasteful to invoke `Bid.all_exceeding` in those cases.
    def legal_calls(self) -> Sequence[Call]:
        if self.status is not Auction.Incomplete:
            return []

        # It's always legal to pass.
        possibilities = [Pass]

        # It's always legal to make a bid greater than the last bid, unless that was 7 notrump.
        possibilities.extend(Bid.all_exceeding(self.max_bid))

        if self.last_located_bid is not None:
            _assert_type(self.last_located_bid.player, Player)
            last_bidder: Player = self.last_located_bid.player
            last_caller: Player = self.player_calls[-1].player

            current_caller: Player = self.table.get_lho(last_caller)
            # It's legal to double the last bid, if it was made by one of your opponents.
            if self.table.are_opponents(current_caller, last_bidder):
                if self.is_doubled is None:
                    possibilities.append(Double)
            # It's legal to redouble an opponent's double.
            elif last_bidder in {current_caller, self.table.get_partner(current_caller)}:
                if self.is_doubled == 2:
                    possibilities.append(Redouble)

        return possibilities

    def random_legal_call(self) -> Call:
        legal_calls = self.legal_calls()

        f = 2.0

        weights = [f ** -(i + 1) for i, _ in enumerate(legal_calls)]

        # OK, now clobber the first entry -- which corresponds to Pass -- so that it's at least as big as all the other
        # entries added up.
        weights[0] = max(weights[0], sum(weights[1:]) * 2)

        return random.choices(legal_calls, weights=weights)[0]

    def fancy_HTML_display(self) -> list[list[Call | str | None]]:
        return self.fancy_two_dimensional_display(as_html=True)

    def fancy_two_dimensional_display(self, as_html: bool = False) -> list[list[Call | str | None]]:
        """Return the auction in table form, with the first column always West, as per <a
        href="http://www.rpbridge.net/p/7z69.pdf">Bridge Writing Style Guide by Richard Pavlicek</a>

        """
        rows: list[list[Call | str | None]] = []
        this_row: list[Call | str | None] = []
        for pc in self.player_calls:
            if not rows and all(call is None for call in this_row):
                # pad the first row at the left with empty slots.
                value = "NESW".index(pc.player.seat.value) + 1
                for _ in range(value % 4):
                    this_row.append(None)

            if len(this_row) < 4:
                this_row.append(pc.call.serialize(as_html=True) if as_html else pc.call)

            if len(this_row) == 4:
                rows.append(this_row)
                this_row = []

        if this_row:
            rows.append(this_row)

        return rows
