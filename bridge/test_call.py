import pytest

from .card import Suit
from .contract import Bid, DeserializationException, Double, Pass, Redouble


def test_serialization() -> None:
    eva_thang = []
    for candidate in Pass, Double, Redouble:
        s = candidate.serialize()
        roundtripped = Bid.deserialize(s)
        assert roundtripped == candidate
        eva_thang.append(s)

    for level in range(1, 7):
        for denomination in [*list(Suit), None]:
            candidate = Bid(level=level, denomination=denomination)
            s = candidate.serialize()
            roundtripped = Bid.deserialize(s)
            assert roundtripped == candidate
            eva_thang.append(s)

    assert (
        " ".join(eva_thang)
        == "Pass Double Redouble 1♣ 1♦ 1♥ 1♠ 1N 2♣ 2♦ 2♥ 2♠ 2N 3♣ 3♦ 3♥ 3♠ 3N 4♣ 4♦ 4♥ 4♠ 4N 5♣ 5♦ 5♥ 5♠ 5N 6♣ 6♦ 6♥ 6♠ 6N"
    )


def test_dont_take_no_shit_from_nobody() -> None:
    with pytest.raises(DeserializationException) as e:
        Bid.deserialize("bogon")
    assert "'bogon'" in str(e.value)
